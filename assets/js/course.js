
//window.location.search returns the query string part of the URL
// console.log(window.location.search)

//instantiate a URLSearchParams object so we can access specific parts of the query string
let params = new URLSearchParams(window.location.search)

let token = localStorage.getItem('token');

let courseId = params.get('courseId')

let adminUser = localStorage.getItem("isAdmin")
let loggedId = localStorage.getItem('id')

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");
let backButton = document.querySelector("#backButton");
let enrolleeList;



fetch(`https://francis-capstone2.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(loggedId)
	courseName.innerHTML = data.name;
	courseDesc.innerHTML = data.description;
	coursePrice.innerHTML = data.price;
	nameOfCourse = data._id;
	let isEnrolled;

	data.enrollees.forEach(enrolledId => {
		if(enrolledId.userId == loggedId){
			isEnrolled = true
		}
	})
	console.log(isEnrolled)
	// console.log(data._id)
	// fetch(`http://localhost:3000/api/courses/${loggedId}/${courseId}`)
	// .then(res => res.json())
	// .then(data => {
	// 	data.enrollees.forEach(enrolledId => {
	// 		if(enrolledId.userId == loggedId){
	// 			isEnrolled = true
	// 		}
	// 	})
		
	// })

	if(isEnrolled == true){
		let enrollButton = `<button id="enrollButton" class="btn btn-block btn-secondary">Enroll</button>`
		let backBut = `<button id="backButton" class="btn btn-secondary mb-5" onclick="document.location='./courses.html'">Back</button>`
		backButton.innerHTML = backBut
		enrollContainer.innerHTML = enrollButton
		document.querySelector('#enrollButton').addEventListener("click", ()=> {
			alert("You have already enrolled in this course!")
			window.location.replace("./courses.html")
		})
	}else if(adminUser === "false" || !adminUser) {
		let enrollButton = `<button id="enrollButton" class="btn btn-block btn-secondary">Enroll</button>`
		let backBut = `<button id="backButton" class="btn btn-secondary mb-5" onclick="document.location='./courses.html'">Back</button>`
		backButton.innerHTML = backBut
		enrollContainer.innerHTML = enrollButton
		document.querySelector('#enrollButton').addEventListener("click", ()=> {
			// console.log("You enrolled!");

			fetch("https://francis-capstone2.herokuapp.com/api/users/enroll", {
				method: "PUT",
				headers: {
					"Content-Type" :"application/json",
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId,
				}),
			})
				.then((res) => {
					return res.json()
				})
				.then((data) => {
					// console.log(data)
					if(data === true) {
						alert("Thank you for enrolling!")
						window.location.replace("./courses.html")
					} else {
						alert("Oops.. Something went wrong! Please ensure you are logged in")
					}
				})
		})
	}else{
		if(data.enrollees.length < 1) {
			enrollContainer.innerHTML = "No enrollees"
			let backBut = `<button id="backButton" class="btn btn-secondary mb-5" onclick="document.location='./courses.html'">Back</button>`
			backButton.innerHTML = backBut
		}else{
			let backBut = `<button id="backButton" class="btn btn-secondary mb-5" onclick="document.location='./courses.html'">Back</button>`
			backButton.innerHTML = backBut
			// document.querySelector('#backButton').addEventListener("click", ()=> {
			// 	window.location.replace('./courses.html')
			// })
			data.enrollees.map(enrol => {
				fetch(`https://francis-capstone2.herokuapp.com/api/users/${enrol.userId}`)
				.then(res => res.json())
				.then(data => {
					enrollContainer.innerHTML += 
					`
						<li class="text-justify">${data.firstName} ${data.lastName}</li>
					`
				})
				return 
			})
		}
	}
})

