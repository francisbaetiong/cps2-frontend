let token = localStorage.getItem("token")
let nameToAppear = document.querySelector("#nameToAppear")
let postReview = document.querySelector("#myReview")

if(!token || token === null){
	alert("You must log in first.")
	window.location.replace('./login.html')
}else{
	fetch('https://francis-capstone2.herokuapp.com/api/users/details', {
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		userId = data._id
		firstName = data.firstName
		lastName = data.lastName
		email = data.email
		mobileNo = data.mobileNo
		nameToAppear.placeholder = data.firstName
		console.log(userId)
		document.querySelector("#leaveReview").addEventListener("submit", (e) => {
		e.preventDefault()

		let reviewComment = postReview.value

		fetch('https://francis-capstone2.herokuapp.com/api/users', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				userId: userId,
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				review: reviewComment
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				window.location.replace('./about.html')
			}else{
				alert("Something went wrong.")
			}
		})
		})
	})
}