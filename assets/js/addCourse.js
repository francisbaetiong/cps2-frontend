let formSubmit = document.querySelector('#createCourse')

// console.log(formSubmit)

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()
	// console.log("You submitted a form!")
	let courseName = document.querySelector('#courseName').value
	let description = document.querySelector('#courseDescription').value
	let price = document.querySelector('#coursePrice').value
	let token = localStorage.getItem('token')
	// console.log(courseName, description, price)
	fetch('https://francis-capstone2.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		// console.log(data)
		if(data === true) {
					alert("Successfully added a course")
					window.location.replace("./courses.html")
				} else {
					alert("Oops.. Something went wrong!")
				}
	})
})