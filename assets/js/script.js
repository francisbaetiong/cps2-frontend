let navItems = document.querySelector("#navSession")
let navItemsProf = document.querySelector("#navSessionProfile")


let userToken = localStorage.getItem("token")

if(!userToken || userToken === null){
	navItems.innerHTML =
		`
		<li class="nav-item">
			<a href="./login.html" class="nav-link">Log In</a>
		</li>
		`

	navItemsProf.innerHTML = 
		`
		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>
		`

		
}else{
	navItems.innerHTML =
	`
	<li class="nav-item">
		<a href="./logout.html" class="nav-link">Log Out</a>
	</li>
	`

	navItemsProf.innerHTML = 
	`
	<li class="nav-item">
		<a href="./profile.html" class="nav-link"> Profile </a>
	</li>
	`

}

