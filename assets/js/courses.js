let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter;
// console.log(adminUser)
//conditional rendering
if(adminUser === "false" || !adminUser){
	modalButton.innerHTML = null
}else{
	modalButton.innerHTML = 
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-secondary">Add Course</a>
	</div>
	`
}

//Fetch the courses from our API
if(adminUser === "false" || !adminUser) {
	fetch('https://francis-capstone2.herokuapp.com/api/courses')
	.then(res => res.json())
	.then(data => {
		// console.log(data)

		let courseData;

		//if the number of courses fetched is less than 1, display no courses
		if(data.length < 1){
			courseData = "No courses available"
		}else{
			//iterate the courses collection and display each course
			courseData = data.map(course => {
					cardFooter = 
					`
					<div class="text-center">
					<a href="./course.html?courseId=${course._id}" class="btn btn-secondary text-white editButton">Select Course</a>
					</div>
					`

			return(
				`
				<div class="col-md-4 my-3">
					<div class="card courseCardFooter">
						<div class="card-body bg-dark text-light cardBodyBg">
							<h5 class="card-title text-center">${course.name}</h5>
							<p class="card-text text-center">
								${course.description}
							</p>
							<p class="card-text text-center">
								PHP ${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>		
					</div>
				</div>
				`
			)
			}).join("")
		}

		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData
})
}else{
	fetch('https://francis-capstone2.herokuapp.com/api/courses/all')
	.then(res => res.json())
	.then(data => {
		// console.log(data[1].isActive)

		let courseData;

		//if the number of courses fetched is less than 1, display no courses
		if(data.length < 1){
			courseData = "No courses available"
		}else{
			//iterate the courses collection and display each course
			courseData = data.map(course => {
					if(course.isActive == true){
						cardFooter = 
						`
						<div class="text-center">
						<a href="./course.html?courseId=${course._id}" class="btn btn-secondary text-white editButton">View Enrollees</a>
						</div>
						<div class="text-center m-1">
						<a href="./editCourse.html?courseId=${course._id}" class="btn btn-secondary text-white editButton px-4">Edit Course</a>
						</div>
						<div class="text-center">
						<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white dangerButton">Disable Course</a>
						</div>
						`
					}else{
						cardFooter =
						`
						<a href="./course.html?courseId=${course._id}" class="btn btn-secondary text-white btn-block editButton">View Enrollees</a>
						<a href="./editCourse.html?courseId=${course._id}" class="btn btn-secondary text-white btn-block editButton">Edit</a>

						<a href="./enableCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block dangerButton">Enable Course</a>
						`
					}
			return(
				`
				<div class="col-md-4 my-3 ">
					<div class="card courseCardFooter">
						<div class="card-body text-light cardBodyBg">
							<h5 class="card-title text-center">${course.name}</h5>
							<p class="card-text text-center">
								${course.description}
							</p>
							<p class="card-text text-center">
								PHP ${course.price}
							</p>
						</div>
						<div class="card-footer">
							${cardFooter}
						</div>		
					</div>
				</div>
				`
			)
			}).join("")
		}

		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData
	})
}