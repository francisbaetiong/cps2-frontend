let params = new URLSearchParams(window.location.search)

let userId = params.get('userId')

console.log(userId)

let firstName = document.querySelector("#editFirstName")
let lastName = document.querySelector("#editLastName")
let email = document.querySelector("#editEmail")
let mobile = document.querySelector("#editMobile")

fetch(`https://francis-capstone2.herokuapp.com/api/users/${userId}`)
.then((res) => res.json())
.then((data) => {
	// console.log(data)

	firstName.placeholder = data.firstName
	lastName.placeholder = data.lastName
	email.placeholder = data.email
	mobile.placeholder = data.mobileNo
	review = data.review

	document.querySelector("#editProfile").addEventListener("submit", (e) => {
		e.preventDefault()

		let nameFirst = firstName.value
		let nameLast = lastName.value
		let emailValue = email.value
		let mobileValue = mobile.value

		let token = localStorage.getItem('token')

		fetch('https://francis-capstone2.herokuapp.com/api/users', {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				userId: userId,
				firstName: nameFirst,
				lastName: nameLast,
				email: emailValue,
				mobileNo: mobileValue,
				review: review
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				window.location.replace('./profile.html')
			}else{
				alert("Something went wrong.")
			}
		})
	})
});
