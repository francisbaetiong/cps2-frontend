let aboutUserToken = localStorage.getItem("token")
let userReview;
let userFirstName;

fetch('https://francis-capstone2.herokuapp.com/api/users')
	.then(res => res.json())
	.then(data => {
		console.log(data)
		let userData;

		userData = data.filter(member => {
			if(member.review == ""){
				return false;
			} 
			return true;
		}).map(member => {
				userReview = member.review
				userFirstName = member.firstName			
		return(
			`
			<div class="carousel-item">
				<div class="card">
				  <div class="card-header bg-dark text-light">
				    Feedback
				  </div>
				  <div id="cardBodyAbout" class="card-body">
				    <blockquote class="blockquote mb-0">
				      <p>${userReview}</p>
				      <footer class="blockquote-footer">${userFirstName}</footer>
				    </blockquote>
				  </div>
				</div>
			</div>

			`
		)
		}).join("")

		let container = document.querySelector("#reviewsContainer")

		container.innerHTML = userData
	})

let memberReview = document.querySelector("#reviewButton")
if(!aboutUserToken || aboutUserToken === null){
	memberReview.innerHTML =
		`
		<div class="text-center mb-5">
		<a href="./login.html" class="btn btn-secondary">Login to leave comment</a>
		</div>
		`
		
}else{
	memberReview.innerHTML =
		`
		<div class="text-center mb-5">
		<a href="./reviewpage.html" class="btn btn-secondary">Leave review</a>
		</div>
		`
}



