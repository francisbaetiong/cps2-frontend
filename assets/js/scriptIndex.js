let navItemsHome = document.querySelector("#navSessionHome")
let navItemsHome2 = document.querySelector("#navSessionHome2")
let userToken = localStorage.getItem("token")
let homePage = document.querySelector("#homePageStart")

if(!userToken || userToken === null){
	navItemsHome.innerHTML =
		`
		<li class="nav-item">
			<a href="./pages/login.html" class="nav-link">Log In</a>
		</li>
		`
	navItemsHome2.innerHTML = 
		`
		<li class="nav-item">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>
		`
	homePageStart.innerHTML = 
		`
		<button id="homePageButton" onclick="document.location='./pages/login.html'" type="button" class="btn btn-secondary btn-lg">Click here to Start</button>
		`

}else{
	navItemsHome.innerHTML =
		`
		<li class="nav-item">
			<a href="./pages/logout.html" class="nav-link">Log Out</a>
		</li>
		`
	navItemsHome2.innerHTML = 
		`
		<li class="nav-item">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>
		`
	homePageStart.innerHTML = 
		`
		<button id="homePageButton" onclick="document.location='./pages/courses.html'" type="button" class="btn btn-secondary btn-lg">Click here to Start</button>
		`
}